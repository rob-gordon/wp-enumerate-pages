console.log(`Getting All Pages on the Site`)
const puppeteer = require('puppeteer')
const fs = require('fs')

const waitFor = ms => new Promise(r => setTimeout(r, ms))
const asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array)
  }
}

const getAllPages = async (page, root) => {
  console.log(`looking into ${root}`)
  await page.goto(root)
  const newList = await page.evaluate(() => {
    return [...document.querySelectorAll('td:first-child a')].map(x => x.href)
  })
  const links = newList.filter(x => x.indexOf('xml') < 0)
  list = list.concat(links)
  await asyncForEach(newList.filter(x => x.indexOf('xml') > -1), async link => {
    await getAllPages(page, link)
  })
}

let list = []
let errors = []
let currentpage = ''
let counter = 0
;(async () => {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  await getAllPages(page, `https://constructive.co/sitemap_index.xml`)
  fs.writeFileSync('pages.json', JSON.stringify(list))
  await browser.close()
})()
